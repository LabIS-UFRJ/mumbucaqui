package com.gabrielmmoraes.mumbucaqui;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;

import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private List<Tuplas<Double,Double>> coordAlimenticios   = new ArrayList<Tuplas<Double,Double>>();
    private List<Tuplas<Double,Double>> coordFarmacos       = new ArrayList<Tuplas<Double,Double>>();
    private List<Tuplas<Double,Double>> coordComercio       = new ArrayList<Tuplas<Double,Double>>();

    private Marcadores marcadoresAlimenticios               = new Marcadores();
    private Marcadores marcadoresFarmacos                   = new Marcadores();
    private Marcadores marcadoresComercio                   = new Marcadores();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Inicializando com foco em Maricá
        LatLng marica = new LatLng(-22.9163,-42.822);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(marica));

        // Definindo Zoom máximo e mínimo do Maps
        mMap.setMinZoomPreference(10.0f);
        mMap.setMaxZoomPreference(17.0f);

        // Retirando o compasso
        UiSettings uisettings = mMap.getUiSettings();
        uisettings.setCompassEnabled(false);

        // Definindo listeners
        definirListenersMapa(mMap);
        definirListenersBotoes();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void definirListenersBotoes() {

        // Seleciona o botão de ajuda
        View botaoAjuda = findViewById(R.id.botaoAjuda);

        // Adiciona listener de toque
        botaoAjuda.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();

                // Se o botão foi pressionado:
                if (action == motionEvent.ACTION_DOWN){
                    mostrarAjuda();
                }

                // Se ele foi liberado:
                else if(action == motionEvent.ACTION_UP){
                    esconderAjuda();
                }

                return true;
            }
        });

        // Seleciona o botão de alimentícios
        View botaoAlimenticios = findViewById(R.id.botaoAlimenticio);

        // Adiciona listener de toque
        botaoAlimenticios.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();

                // Se o botão foi pressionado:
                if (action == motionEvent.ACTION_DOWN){
                    desmarcarFarmacos();
                    desmarcarComercio();
                    marcarAlimenticios();
                }

                return true;
            }
        });

        // Seleciona o botão de fármacos
        View botaoFarmacos = findViewById(R.id.botaoFarmacos);

        // Adiciona listener de toque
        botaoFarmacos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();

                // Se o botão foi pressionado:
                if (action == motionEvent.ACTION_DOWN){
                    desmarcarAlimenticios();
                    desmarcarComercio();
                    marcarFarmacos();
                }

                return true;
            }
        });

        // Seleciona o botão de comércio
        View botaoComercio = findViewById(R.id.botaoComercio);

        // Adiciona listener de toque
        botaoComercio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();

                // Se o botão foi pressionado:
                if (action == motionEvent.ACTION_DOWN) {
                    desmarcarAlimenticios();
                    desmarcarFarmacos();
                    marcarComercio();
                }

                return true;
            }
        });

    }

    private void marcarAlimenticios(){

        Tuplas<Double,Double> tuplaCoord = new Tuplas<>(-22.9163,-42.822);
        coordAlimenticios.add(tuplaCoord);

        marcadoresAlimenticios.addMarcadores(coordAlimenticios, mMap);

    }

    private void marcarFarmacos(){

        Tuplas<Double,Double> tuplaCoord2 = new Tuplas<>(-22.8163,-42.822);
        coordFarmacos.add(tuplaCoord2);

        Tuplas<Double,Double> tuplaCoord3 = new Tuplas<>(-22.8163,-42.722);
        coordFarmacos.add(tuplaCoord3);

        marcadoresFarmacos.addMarcadores(coordFarmacos, mMap);

    }

    private void marcarComercio(){

        Tuplas<Double,Double> tuplaCoord4 = new Tuplas<>(-23.0163,-42.822);
        coordComercio.add(tuplaCoord4);

        marcadoresComercio.addMarcadores(coordComercio, mMap);

    }

    private void desmarcarAlimenticios(){
        marcadoresAlimenticios.removerMarcadores();
    }

    private void desmarcarFarmacos(){
        marcadoresFarmacos.removerMarcadores();
    }

    private void desmarcarComercio(){
        marcadoresComercio.removerMarcadores();
    }

    // Torna as legendas dos botões visíveis
    private void mostrarAjuda() {
        View descAlimenticios = findViewById(R.id.descAlimenticios);
        descAlimenticios.setVisibility(View.VISIBLE);

        View descFarmacos = findViewById(R.id.descFarmacos);
        descFarmacos.setVisibility(View.VISIBLE);

        View descComercio = findViewById(R.id.descComercio);
        descComercio.setVisibility(View.VISIBLE);
    }

    // Torna as legendas dos botões invisíveis
    private void esconderAjuda() {
        View descAlimenticios = findViewById(R.id.descAlimenticios);
        descAlimenticios.setVisibility(View.INVISIBLE);

        View descFarmacos = findViewById(R.id.descFarmacos);
        descFarmacos.setVisibility(View.INVISIBLE);

        View descComercio = findViewById(R.id.descComercio);
        descComercio.setVisibility(View.INVISIBLE);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void definirListenersMapa(GoogleMap mMap) {

        // No caso de movimento no mapa
        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                if(GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE == i){
                    // esconderBotoes();
                }
            }
        });

        // Caso a câmera esteja parada
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                // mostrarBotoes();
            }
        });
    }

    // Esconde botões com animação de Fade Out (bugado)
    private void esconderBotoes() {
        View botaoAlimenticio = findViewById(R.id.botaoAlimenticio);
        botaoAlimenticio.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out));

        View botaoFarmacos = findViewById(R.id.botaoFarmacos);
        botaoFarmacos.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out));

        View botaoComercio = findViewById(R.id.botaoComercio);
        botaoComercio.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out));
    }

    // Mostra botões com animação de Fade In (bugado)
    private void mostrarBotoes() {
        View botaoAlimenticio = findViewById(R.id.botaoAlimenticio);
        botaoAlimenticio.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));

        View botaoFarmacos = findViewById(R.id.botaoFarmacos);
        botaoFarmacos.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));

        View botaoComercio = findViewById(R.id.botaoComercio);
        botaoComercio.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
    }
}
