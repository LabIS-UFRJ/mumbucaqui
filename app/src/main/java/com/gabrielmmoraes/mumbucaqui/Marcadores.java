package com.gabrielmmoraes.mumbucaqui;

import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class Marcadores {

    public List<Marker> listaMarcadores = new ArrayList<Marker>();

    public Marcadores(){

    }

    public Marcadores (List<Tuplas<Double,Double> > coordenadas, GoogleMap mMap){

        int tamanhoLista = coordenadas.size();

        for (int i = 0; i < tamanhoLista; i++){

            Tuplas<Double,Double> coordAtual = coordenadas.get(i);
            Double xAtual = coordAtual.getX();
            Double yAtual = coordAtual.getY();
            LatLng coordMarcador = new LatLng(xAtual, yAtual);

            Marker marcador = mMap.addMarker(new MarkerOptions().position(coordMarcador));

            Log.d("ADD MARCADOR", ""+Double.toString(xAtual)+" "+Double.toString(yAtual));

            listaMarcadores.add(marcador);
        }
    }

    public void addMarcador (Tuplas<Double,Double> coordenada, GoogleMap mMap){
        Double xAtual = coordenada.getX();
        Double yAtual = coordenada.getY();
        LatLng coordMarcador = new LatLng(xAtual, yAtual);

        Marker marcador = mMap.addMarker(new MarkerOptions().position(coordMarcador));

        listaMarcadores.add(marcador);
    }

    public void addMarcadores (List<Tuplas<Double,Double> > coordenadas, GoogleMap mMap){

        int tamanhoLista = coordenadas.size();

        for (int i = 0; i < tamanhoLista; i++){

            Tuplas<Double,Double> coordAtual = coordenadas.get(i);
            Double xAtual = coordAtual.getX();
            Double yAtual = coordAtual.getY();
            LatLng coordMarcador = new LatLng(xAtual, yAtual);

            Marker marcador = mMap.addMarker(new MarkerOptions().position(coordMarcador));

            Log.d("ADD MARCADOR", ""+Double.toString(xAtual)+" "+Double.toString(yAtual));

            listaMarcadores.add(marcador);
        }
    }

    public void removerMarcadores(){
        for (Marker marcador: listaMarcadores){
            marcador.remove();
        }

        listaMarcadores.clear();
    }
}
