package com.gabrielmmoraes.mumbucaqui;

public class Tuplas<X, Y> {

    private X x;

    private Y y;

    public Tuplas(X x, Y y) {
        this.x = x;
        this.y = y;
    }

    public X getX(){
        return x;
    }

    public Y getY(){
        return y;
    }

    public void setX(X x){
        this.x = x;
        return;
    }

    public void setY(Y y){
        this.y = y;
        return;
    }
}
